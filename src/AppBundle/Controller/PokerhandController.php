<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Pokerhand;

class PokerhandController extends Controller
{

    /**
     * @Route("/{pack}", name="pokerhand", defaults={"pack" = 1}, requirements={"pack": "\d+"})
     */
    public function indexAction(Request $request, int $pack)
    {

        $dealer = new Pokerhand\Dealer($pack);
        $packNumber = $dealer->getDeckNumber();
        $hand = $dealer->getHand();

        return $this->render('pokerhand/index.html.twig', [
                  'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
                  'hand' => $hand,
                  'pack_number' => $packNumber
        ]);
    }

}
