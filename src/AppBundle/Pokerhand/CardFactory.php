<?php

namespace AppBundle\Pokerhand;

/**
 * CardFactory is used for generate Card objects, based on suit and rank
 *
 * @author popovicsm
 */
class CardFactory
{
    const SUIT_SPADES = 0;
    const SUIT_HEARTS = 1;
    const SUIT_DIAMONDS = 2;
    const SUIT_CLUBS = 3;

    /**
     * 
     * @param int $suit
     * @param int $rank
     * @return \AppBundle\Pokerhand\CardAbstract
     * @throws \InvalidArgumentException
     */
    public function create(int $suit, int $rank): CardAbstract
    {
        switch ($suit) {
            case self::SUIT_SPADES:
                return new CardSpades($rank);
            case self::SUIT_HEARTS:
                return new CardHearts($rank);
            case self::SUIT_DIAMONDS:
                return new CardDiamonds($rank);
            case self::SUIT_CLUBS:
                return new CardClubs($rank);
            default:
                throw new \InvalidArgumentException("{$suit} is not a valid suit");
        }
    }

}
