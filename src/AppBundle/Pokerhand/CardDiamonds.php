<?php

namespace AppBundle\Pokerhand;

/**
 * Card suit of diamonds
 *
 * @author popovicsm
 */
class CardDiamonds extends CardAbstract
{
    protected $color = "#FF0000";
    protected $name = "Diamonds";
    protected $cssName = "suitdiamonds";
    protected $rankName;
    protected $rank;
    protected $rankSign;
    
    /**
     * 
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        parent::__construct($rank);
    }
}
