<?php

namespace AppBundle\Pokerhand;

/**
 * Card suit of hearts
 *
 * @author popovicsm
 */
class CardHearts extends CardAbstract
{
    protected $color = "#FF0000";
    protected $name = "Hearts";
    protected $cssName = "suithearts";
    protected $rankName;
    protected $rank;
    protected $rankSign;
    
    /**
     * 
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        parent::__construct($rank);
    }
}