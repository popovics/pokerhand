<?php

namespace AppBundle\Pokerhand;

/**
 * Hand object is for creating hand objects, which contains score of the hand, card properties. 
 *
 * @author popovicsm
 */
class Hand
{

    const SCORE_ROYAL_FLUSH = 10;
    const SCORE_STRAIGHT_FLUSH = 9;
    const SCORE_FOUR_OF_A_KIND = 8;
    const SCORE_FULL_HOUSE = 7;
    const SCORE_FLUSH = 6;
    const SCORE_STRAIGHT = 5;
    const SCORE_THREE_OF_A_KIND = 4;
    const SCORE_TWO_PAIRS = 3;
    const SCORE_ONE_PAIR = 2;
    const SCORE_NO_PAIR = 1;
    
    const NAME_ROYAL_FLUSH = "Royal Flush";
    const NAME_STRAIGHT_FLUSH = "Straight Flush";
    const NAME_FOUR_OF_A_KIND = "Four of a kind";
    const NAME_FULL_HOUSE = "Full House";
    const NAME_FLUSH = "Flush";
    const NAME_STRAIGHT = "Straight";
    const NAME_THREE_OF_A_KIND = "Three of a kind";
    const NAME_TWO_PAIRS = "Two pairs";
    const NAME_ONE_PAIR = "One pair";
    const NAME_NO_PAIR = "No pair";
    
    
    const ACE_HIGHRANK = 13;
    const ACE_LOWRANK = 0;
    
    private $cards = [];
    private $score = [];
    private $rankCollection = [];
    private $highestCard = [];
    
    private $cardObjects = [];
    private $highestCardObject;
    
    private $higherCards;
    
    /**
     * 
     * @param array $cards
     * @param bool $higherCards
     */
    public function __construct(array $cards, bool $higherCards = false)
    {
        $this->higherCards = $higherCards;
        $this->cards = $cards;
        $this->setAceRank();
        $this->setHighestCard();
        $this->setRankCollection();
        $this->setScore();
        $this->setCardObjects();
        $this->setHighestCardObject();
        
    }
    
    /**
     * 
     * @return array
     */
    public function getCardObjects(): array
    {
        return $this->cardObjects;
    }
    
    /**
     * 
     * @return \AppBundle\Pokerhand\CardAbstract
     */
    public function getHighestCardObject(): CardAbstract
    {
        return $this->highestCardObject;
    }
    
    /**
     * Set card objects based on CardAbstract classes by CardFactory
     */
    private function setCardObjects()
    {
        foreach ($this->cards as $card) {
            $factory = new CardFactory();
            $this->cardObjects[] = $factory->create($card["suit"], $card["rank"]);
        }
    }
    
    
    /**
     * Set up object of card which has the highest rank
     */
    private function setHighestCardObject() 
    {
        $factory = new CardFactory();
        $this->highestCardObject = $factory->create($this->highestCard["suit"], $this->highestCard["rank"]);
    }
    
    /**
     * Return cards (5 given cards)
     * @return array
     */
    public function getCards(): array
    {
        return $this->cards;
    }
    
    /*
     * Return calculated score of hand
     */
    public function getScore()
    {
        return $this->score;
    }
    
    /**
     * Set rank of Ace, it can be 0 or 13 based on given $higherCards parameter in constructor
     */
    private function setAceRank() 
    {
        $newCards = [];
        if ($this->higherCards) {
            foreach ($this->cards as $card) {
                if ($card["rank"] == self::ACE_LOWRANK) {
                  $card["rank"] = self::ACE_HIGHRANK;
                }
                $newCards[] = $card;
            }
            $this->cards = $newCards;
        }
    }
    
    /**
     * Score set based on suits and ranks
     */
    private function setScore()
    {
        if ($this->isRoyalFlush()) {
            return;
        } 
        if ($this->isStraightFlush()) {
            return;
        }
        if ($this->isFourOfAKind()) {
            return;
        }
        if ($this->isFullHouse()) {
            return;
        }
        if ($this->isFlush()) {
            return;
        }
        if ($this->isStraight()) {
            return;
        }
        if ($this->isThreeOfAKind()) {
            return;
        }
        if ($this->isTwoPairs()) {
            return;
        }
        if ($this->isOnePair()) {
            return;
        }
        $highestCard = $this->getHighestCard();
        $this->score = [self::SCORE_NO_PAIR, $highestCard["rank"], self::NAME_NO_PAIR];
    }
    
    /**
     * 
     * @return bool
     */
    private function isRoyalFlush(): bool
    {
        if (!$this->isFlush()) {
            return false;
        }
        
        if (!$this->isStraight()) {
            return false;
        }
        
        $highestCard = $this->getHighestCard();
        
        if ($highestCard["rank"] != 13) {
          return false;
        }
        
        $this->score = [self::SCORE_ROYAL_FLUSH, 13, self::NAME_ROYAL_FLUSH];
        return true;
    }
    
    /**
     * 
     * @return bool
     */
    private function isStraightFlush(): bool
    {
        if (!$this->isFlush()) {
            return false;
        }
        
        if (!$this->isStraight()) {
            return false;
        }
        
        $highestCard = $this->getHighestCard();
        
        $this->score = [self::SCORE_STRAIGHT_FLUSH, $highestCard["rank"], self::NAME_STRAIGHT_FLUSH];
        return true;
    }

    /**
     * 
     * @return bool
     */
    private function isFlush(): bool
    {
        $suits = array_unique(array_column($this->cards, 'suit'));
        if (count($suits) != 1) {
            return false;
        }
        
        $highestCard = $this->getHighestCard();
        
        $this->score = [self::SCORE_FLUSH, $highestCard["rank"], self::NAME_FLUSH];
        return true;
    }

    /**
     * 
     * @return bool
     */
    private function isStraight(): bool
    {
        $ranks = array_column($this->cards, 'rank');
        sort($ranks, SORT_NUMERIC);
        for ($rankIndex = 0; $rankIndex < (count($ranks) - 1); $rankIndex++) {
            if ($ranks[$rankIndex] + 1 != $ranks[$rankIndex + 1]) {
                return false;
            }
        }
        
        $highestCard = $this->getHighestCard();
        
        $this->score = [self::SCORE_STRAIGHT, $highestCard["rank"], self::NAME_STRAIGHT];
        return true;
    }
    
    /**
     * Four of a kind is a special case if we use more than one deck. 
     * In that case there is possible to be 5 cards with same rank. This method will handle this as a simple four of a kind. 
     * 
     * @return bool
     */
    private function isFourOfAKind(): bool
    {
        if (count($this->rankCollection) != 2 && count($this->rankCollection) != 1) {
            return false;
        }
        
        if ($this->rankCollection[0]['occurrence'] < 4) {
            return false;
        }
        
        
        $factory = new CardFactory();
        $cardObject = $factory->create(0, $this->rankCollection[0]["rank"]);
        $name = self::NAME_FOUR_OF_A_KIND . " (" . $cardObject->getRankName() . ")";
        
        $this->score = [self::SCORE_FOUR_OF_A_KIND, $this->rankCollection[0]["rank"], $name];
        return true;
    }
    
    /**
     * 
     * @return bool
     */
    private function isFullHouse(): bool
    {
        if (count($this->rankCollection) != 2) {
            return false;
        }
        
        if ($this->rankCollection[0]['occurrence'] != 3) {
            return false;
        }
        
        if ($this->rankCollection[1]['occurrence'] != 2) {
            return false;
        }
        
        $factory = new CardFactory();
        $cardObject1 = $factory->create(0, $this->rankCollection[0]["rank"]);
        $cardObject2 = $factory->create(0, $this->rankCollection[1]["rank"]);
        
        $name = self::NAME_FULL_HOUSE . " (" . $cardObject1->getRankName() . " and " . $cardObject2->getRankName() . ")";
        
        $this->score = [self::SCORE_FULL_HOUSE, $this->rankCollection[0]["rank"], $name];
        return true;
    }
    
    /**
     * 
     * @return bool
     */
    private function isThreeOfAKind(): bool
    {
        if (count($this->rankCollection) != 3) {
            return false;
        }
      
        if ($this->rankCollection[0]['occurrence'] != 3) {
            return false;
        }
        
        $factory = new CardFactory();
        $cardObject = $factory->create(0, $this->rankCollection[0]["rank"]);
        $name = self::NAME_THREE_OF_A_KIND . " (" . $cardObject->getRankName() . ")";
        
        $this->score = [self::SCORE_THREE_OF_A_KIND, $this->rankCollection[0]["rank"], $name];
        return true;
    }
    
    /**
     * 
     * @return bool
     */
    private function isTwoPairs(): bool
    {
        if (count($this->rankCollection) != 3) {
            return false;
        }
      
        if ($this->rankCollection[0]['occurrence'] != 2) {
            return false;
        }
        
        if ($this->rankCollection[1]['occurrence'] != 2) {
            return false;
        }
        
        if ($this->rankCollection[0]["rank"] >= $this->rankCollection[1]["rank"]) {
          $rank = $this->rankCollection[0]["rank"];
        } else {
          $rank = $this->rankCollection[1]["rank"];
        }
        
        $factory = new CardFactory();
        $cardObject1 = $factory->create(0, $this->rankCollection[0]["rank"]);
        $cardObject2 = $factory->create(0, $this->rankCollection[1]["rank"]);
        
        $name = self::NAME_TWO_PAIRS . " (" . $cardObject1->getRankName() . ", " . $cardObject2->getRankName() . ")";
        
        $this->score = [self::SCORE_TWO_PAIRS, $rank, $name];
        return true;
    }
    
    /**
     * 
     * @return bool
     */
    private function isOnePair(): bool
    {
        if (count($this->rankCollection) != 4) {
            return false;
        }
      
        if ($this->rankCollection[0]['occurrence'] != 2) {
            return false;
        }
        
        $factory = new CardFactory();
        $cardObject = $factory->create(0, $this->rankCollection[0]["rank"]);
        $name = self::NAME_ONE_PAIR . " (" . $cardObject->getRankName() . ")";
        
        $this->score = [self::SCORE_ONE_PAIR, $this->rankCollection[0]["rank"], $name];
        return true;
    }

    /**
     * Return card with highest rank
     * @return array
     */
    private function getHighestCard(): array
    {
        return $this->highestCard;
    }

    /**
     * Set card with highest rank
     */
    private function setHighestCard() 
    {
        $ranks = array_column($this->cards, "rank");
        $cards = $this->cards;
        array_multisort($ranks, SORT_ASC, $cards, SORT_ASC);
        $this->highestCard = array_pop($cards);
    }

    /**
     * Making groups of same rank cards
     * It helps determine highest hand variation
     */
    private function setRankCollection()
    {
        foreach ($this->cards as $card) {
            if (!isset($rankCollection[$card["rank"]])) {
                $rankGroup = ["rank" => $card["rank"], "occurrence" => 1, "cards" => [$card]];
                $rankCollection[$card["rank"]] = $rankGroup;
            } else {
                $cards = $rankCollection[$card["rank"]]["cards"];
                $occurrence = $rankCollection[$card["rank"]]["occurrence"];

                $cards[] = $card;
                $occurrence++;

                $rankGroup = ["rank" => $card["rank"], "occurrence" => $occurrence, "cards" => $cards];

                $rankCollection[$card["rank"]] = $rankGroup;
            }
        }

        $occurrences = array_column($rankCollection, "occurrence");

        array_multisort($occurrences, SORT_DESC, $rankCollection, SORT_DESC);
        $this->rankCollection = $rankCollection;
    }

}