<?php

namespace AppBundle\Pokerhand;

/**
 * Dealer creates a shuffled card set based on given deck number property. 
 *
 * @author popovicsm
 */
class Dealer
{

    const HAND_CARDS_NUMBER = 5;

    private $deckNumber;
    private $decks = [];
    private $shuffledCards = [];
    private $hand;

    /**
     * It creates deck(s) and shuffles the cards, generates a hand by Hand class. 
     * @param type $deckNumber
     */
    public function __construct($deckNumber)
    {
        $this->deckNumber = $deckNumber;
        $this->setDecks();
        $this->shuffleCards();
        $this->setHand();
    }

  /**
   * Create decks
   */
  private function setDecks()
  {
      for ($deckIndex = 0; $deckIndex < $this->deckNumber; $deckIndex++) {
          $this->decks[] = new Deck();
      }
  }

  /**
   * Shuffle cards of decks
   */
  private function shuffleCards()
  {
      foreach ($this->decks as $deck) {
          array_push($this->shuffledCards, ...$deck->getCards());
      }
      shuffle($this->shuffledCards);
  }

  /**
   * Create Hand objects based on generated 5 cards hand array. 
   * Hand array is created by pulling out 5 cards from shuffled card array. 
   * Every hand has an alternate version which has higher score for aces. 
   * The created two Hand objects has a score based on poker hand rules. Highest ranked hand will be used afterward.
   */
  private function setHand()
  {
      $hand = [];
      for ($popIndex = 0; $popIndex < self::HAND_CARDS_NUMBER; $popIndex++) {
          $hand[] = array_pop($this->shuffledCards);
      }
      
      $lowCardHand = new Hand($hand);
      $highCardHand = new Hand($hand, true);
      
      $lowCardScore = $lowCardHand->getScore();
      $highCardScore = $highCardHand->getScore();
      
      if ($lowCardScore[0] > $highCardScore[0]) {
          $this->hand = $lowCardHand;
      } elseif ($lowCardScore[0] < $highCardScore[0]) {
          $this->hand = $highCardHand;
      } elseif ($lowCardScore[1] <= $highCardScore[1]) {
          $this->hand = $highCardHand;
      } else {
          $this->hand = $lowCardHand;
      }
  }
  
  /**
   * Return shuffled cards
   * @return array
   */
  public function getShuffledCards(): array
  {
      return $this->shuffledCards;
  }
  
  /**
   * Return number of decks
   * @return int
   */
  public function getDeckNumber(): int
  {
      return $this->deckNumber;
  }
  
  /**
   * Return generated Hand object (only the high ranked)
   * @return \AppBundle\Pokerhand\Hand
   */
  public function getHand(): Hand
  {
      return $this->hand;
  }
}