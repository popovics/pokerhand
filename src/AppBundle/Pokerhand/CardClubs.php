<?php

namespace AppBundle\Pokerhand;

/**
 * Card suit of clubs
 *
 * @author popovicsm
 */
class CardClubs extends CardAbstract
{
    protected $color = "#000000";
    protected $name = "Clubs";
    protected $cssName = "suitclubs";
    protected $rankName;
    protected $rank;
    protected $rankSign;
    
    /**
     * 
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        parent::__construct($rank);
    }
}