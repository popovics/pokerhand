<?php

namespace AppBundle\Pokerhand;

/**
 * Abstract class for card suits
 * @author popovicsm
 */
abstract class CardAbstract
{
    protected $color;
    protected $name;
    protected $cssName;
    protected $rankName;
    protected $rankSign;
    protected $rank;
    
    /**
     * Rank is the card rank
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        $this->rank = $rank;
        
        $this->setRankName();
    }
    
    private function setRankName() {
        switch ($this->rank) {
            case 0:
            case 13:
                $this->rankName = "Ace";
                $this->rankSign = "A";
                break;
            case 10:
                $this->rankName = "Jack";
                $this->rankSign = "J";
                break;
            case 11:
                $this->rankName = "Queen";
                $this->rankSign = "Q";
                break;
            case 12:
                $this->rankName = "King";
                $this->rankSign = "K";
                break;
            default:
                $rankName = ($this->rank + 1);
                settype($rankName, "string");
                $this->rankName = $this->rankSign = $rankName;
        }
    }
    
    /**
     * Return name of the suit
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * Return CSS name of the rank
     * @return string
     */
    public function getCssName(): string
    {
        return $this->cssName;
    }
    
    /**
     * Return name of rank (King, 2, Ace, etc.)
     * @return string
     */
    public function getRankName(): string
    {
        return $this->rankName;
    }
    
    /**
     * Return sign of the rank (K, 2, A, erc.)
     * @return string
     */
    public function getRankSign(): string
    {
        return $this->rankSign;
    }
    
    /**
     * Return rank in raw formula
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }
    
    /**
     * 
     * Return color of suit
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
