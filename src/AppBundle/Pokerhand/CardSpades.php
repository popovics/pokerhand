<?php

namespace AppBundle\Pokerhand;

/**
 * Card suit of spades
 *
 * @author popovicsm
 */
class CardSpades extends CardAbstract
{
    protected $color = "#000000";
    protected $name = "Spades";
    protected $cssName = "suitspades";
    protected $rankName;
    protected $rank;
    protected $rankSign;
    
    /**
     * 
     * @param int $rank
     */
    public function __construct(int $rank)
    {
        parent::__construct($rank);
    }
}
