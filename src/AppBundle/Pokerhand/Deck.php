<?php

namespace AppBundle\Pokerhand;

/**
 * PokerhandDeck is a 52-card pack, 4 suits and 14 cards per suits
 *
 * @author popovicsm
 */
class Deck
{

    const SUIT_CARDS_NUMBER = 13;
    const SUITS_NUMBER = 4;

    protected $cards = [];

    public function __construct()
    {
        $this->collectCards();
    }
    
    /**
     * Collecting cards by fill up cards array 
     * Each cards has a suit and ran property
     */
    private function collectCards()
    {
        for ($suit = 0; $suit < self::SUITS_NUMBER; $suit++) {
            for ($serialNumber = 0; $serialNumber < self::SUIT_CARDS_NUMBER; $serialNumber++) {
                $this->cards[] = ['suit' => $suit, 'rank' => $serialNumber];
            }
        }
    }

    /**
     * Return array of cards  
     * @return array
     */
    public function getCards(): array
    {
        return $this->cards;
    }

}
