<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PokerhandControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $deckNumber = 1;
        $client = static::createClient();
        $crawler = $client->request('GET', "/{$deckNumber}");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(5, $crawler->filter('div.card')->count());
        $this->assertContains("Number of packs: {$deckNumber}", $crawler->filter('div.header')->text());
    }
}