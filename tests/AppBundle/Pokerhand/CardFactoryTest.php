<?php

namespace Tests\AppBundle\Pokerhand;

use AppBundle\Pokerhand\CardFactory;
use PHPUnit\Framework\TestCase;

class CardFactoryTest extends TestCase
{
    
    public function testNameIsSpades()
    {
        $factory = new CardFactory();
        $card = $factory->create(0, 1);
        $this->assertEquals("Spades", $card->getName());
    }
    
    public function testNameIsHearts()
    {
        $factory = new CardFactory();
        $card = $factory->create(1, 1);
        $this->assertEquals("Hearts", $card->getName());
    }
    
    public function testNameIsDiamonds()
    {
        $factory = new CardFactory();
        $card = $factory->create(2, 1);
        $this->assertEquals("Diamonds", $card->getName());
    }
    
    public function testNameIsClubs()
    {
        $factory = new CardFactory();
        $card = $factory->create(3, 1);
        $this->assertEquals("Clubs", $card->getName());
    }

    public function testRankIsKing()
    {
        $factory = new CardFactory();
        $card = $factory->create(0, 12);
        $this->assertEquals("King", $card->getRankName());
    }
    
    public function testRankSignIsJ()
    {
        $factory = new CardFactory();
        $card = $factory->create(0, 10);
        $this->assertEquals("J", $card->getRankSign());
    }
    
    public function testColorIsRed()
    {
        $factory = new CardFactory();
        $card = $factory->create(1, 10);
        $this->assertEquals("#FF0000", $card->getColor());
    }
    
    public function testCss() 
    {
        $factory = new CardFactory();
        $card = $factory->create(2, 10);
        $this->assertEquals("suitdiamonds", $card->getCssName());
    }
    
    public function testRankIsTen()
    {
        $factory = new CardFactory();
        $card = $factory->create(0, 10);
        $this->assertEquals(10, $card->getRank());
    }
    
}
