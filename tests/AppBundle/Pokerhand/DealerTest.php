<?php
namespace Tests\AppBundle\Pokerhand;

use AppBundle\Pokerhand\Dealer;
use PHPUnit\Framework\TestCase;

class DealerTest extends TestCase
{
    public function testHand()
    {
        $dealer = new Dealer(1);
        $this->assertInstanceOf("\AppBundle\Pokerhand\Hand", $dealer->getHand());
    }
    
    public function testDeckNumber()
    {
        $deckNumber = 2;
        $dealer = new Dealer($deckNumber);
        $this->assertEquals($deckNumber, $dealer->getDeckNumber());
    }
    
    public function testShuffledCardNumber()
    {
        $deckNumber = 3;
        $restOfCards = $deckNumber * 13 * 4 - 5;
        $dealer = new Dealer($deckNumber);
        $this->assertEquals($restOfCards, count($dealer->getShuffledCards()));
    }
}
