<?php

namespace Tests\AppBundle\Pokerhand;

use AppBundle\Pokerhand\Hand;
use PHPUnit\Framework\TestCase;

class HandTest extends TestCase
{
    public function testHandHasOnePair()
    {
        $cards[] = ['suit' => 0, 'rank' => 1];
        $cards[] = ['suit' => 1, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 11];
        $cards[] = ['suit' => 3, 'rank' => 11];
        $cards[] = ['suit' => 0, 'rank' => 7];
        
        $hand = new Hand($cards);
        $this->assertEquals(2, $hand->getScore()[0]);
    }
    
    public function testHandHasTwoPairs()
    {
        $cards[] = ['suit' => 0, 'rank' => 10];
        $cards[] = ['suit' => 1, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 3];
        $cards[] = ['suit' => 3, 'rank' => 11];
        $cards[] = ['suit' => 0, 'rank' => 11];
        
        $hand = new Hand($cards);
        $this->assertEquals(3, $hand->getScore()[0]);
    }
    
    public function testHandHasThreeOfAKind()
    {
        $cards[] = ['suit' => 0, 'rank' => 2];
        $cards[] = ['suit' => 1, 'rank' => 2];
        $cards[] = ['suit' => 2, 'rank' => 2];
        $cards[] = ['suit' => 3, 'rank' => 11];
        $cards[] = ['suit' => 0, 'rank' => 7];
        
        $hand = new Hand($cards);
        $this->assertEquals(4, $hand->getScore()[0]);
    }
    
    public function testHandHasStraight()
    {
        $cards[] = ['suit' => 0, 'rank' => 1];
        $cards[] = ['suit' => 1, 'rank' => 2];
        $cards[] = ['suit' => 2, 'rank' => 3];
        $cards[] = ['suit' => 3, 'rank' => 4];
        $cards[] = ['suit' => 0, 'rank' => 5];
        
        $hand = new Hand($cards);
        $this->assertEquals(5, $hand->getScore()[0]);
    }
    
    public function testHandHasFlush()
    {
        $cards[] = ['suit' => 0, 'rank' => 1];
        $cards[] = ['suit' => 0, 'rank' => 10];
        $cards[] = ['suit' => 0, 'rank' => 3];
        $cards[] = ['suit' => 0, 'rank' => 11];
        $cards[] = ['suit' => 0, 'rank' => 7];
        
        $hand = new Hand($cards);
        $this->assertEquals(6, $hand->getScore()[0]);
    }
    
    public function testHandHasFullHouse()
    {
        $cards[] = ['suit' => 0, 'rank' => 1];
        $cards[] = ['suit' => 1, 'rank' => 1];
        $cards[] = ['suit' => 2, 'rank' => 4];
        $cards[] = ['suit' => 3, 'rank' => 4];
        $cards[] = ['suit' => 0, 'rank' => 4];
        
        $hand = new Hand($cards);
        $this->assertEquals(7, $hand->getScore()[0]);
    }
    
    public function testHandHasFourOfAKind()
    {
        $cards[] = ['suit' => 0, 'rank' => 10];
        $cards[] = ['suit' => 1, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 10];
        $cards[] = ['suit' => 3, 'rank' => 11];
        $cards[] = ['suit' => 0, 'rank' => 10];
        
        $hand = new Hand($cards);
        $this->assertEquals(8, $hand->getScore()[0]);
    }
    
    public function testHandHasFourOfAKindWithFiveSameRankCard()
    {
        $cards[] = ['suit' => 0, 'rank' => 10];
        $cards[] = ['suit' => 1, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 10];
        $cards[] = ['suit' => 3, 'rank' => 10];
        $cards[] = ['suit' => 0, 'rank' => 10];
        
        $hand = new Hand($cards);
        $this->assertEquals(8, $hand->getScore()[0]);
    }
    
    public function testHandHasStraightFlush()
    {
        $cards[] = ['suit' => 2, 'rank' => 5];
        $cards[] = ['suit' => 2, 'rank' => 6];
        $cards[] = ['suit' => 2, 'rank' => 8];
        $cards[] = ['suit' => 2, 'rank' => 9];
        $cards[] = ['suit' => 2, 'rank' => 7];
        
        $hand = new Hand($cards);
        $this->assertEquals(9, $hand->getScore()[0]);
    }
    
    public function testHandHasRoyalFlush()
    {
        $cards[] = ['suit' => 2, 'rank' => 13];
        $cards[] = ['suit' => 2, 'rank' => 11];
        $cards[] = ['suit' => 2, 'rank' => 12];
        $cards[] = ['suit' => 2, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 9];
        
        $hand = new Hand($cards);
        $this->assertEquals(10, $hand->getScore()[0]);
    }
    
    public function testHandHasNoPair()
    {
        $cards[] = ['suit' => 2, 'rank' => 5];
        $cards[] = ['suit' => 0, 'rank' => 6];
        $cards[] = ['suit' => 2, 'rank' => 10];
        $cards[] = ['suit' => 2, 'rank' => 9];
        $cards[] = ['suit' => 2, 'rank' => 7];
        
        $hand = new Hand($cards);
        $this->assertEquals(1, $hand->getScore()[0]);
    }
    
}
