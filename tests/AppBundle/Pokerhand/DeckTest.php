<?php

namespace Tests\AppBundle\Pokerhand;

use AppBundle\Pokerhand\Deck;
use PHPUnit\Framework\TestCase;

class DeckTest extends TestCase
{
    public function testCardNumber() 
    {
        $deck = new Deck();
        $this->assertEquals(52, count($deck->getCards()));
    }
}
